from PIL import Image
from rtmidi import MidiOut
from time import sleep
from threading import Thread

img = Image.open('test.jpg')
rgb = list(img.convert('RGB').getdata())
hsv = list(img.convert('HSV').getdata())

def player(mout, channel, pixels):
    midiNoteOn = 0b10010000
    midiNoteOff = 0b10000000
    midiCHannel = bin(channel)

    for data in pixels:
        note = data[0]//4+32
        velocity = data[1]//4+64
        duration = data[2]/255/4+0.25
        print(channel,note,velocity,duration)
        mout.send_message([midiNoteOn+channel, note, velocity])
        sleep(duration)
        mout.send_message([midiNoteOff+channel, note, 0])

mout = MidiOut()
mout.open_port(1)

Thread(target=player,args=(mout,0,hsv)).start()
Thread(target=player,args=(mout,1,rgb)).start()

while True:
    sleep(1)